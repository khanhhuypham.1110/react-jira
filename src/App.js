
import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { CREATE_PROJECT, NOT_FOUND_PAGE, PROJECT_DASHBOARD, PROJECT_MANAGEMENT, SIGN_IN, SIGN_UP } from './utils/constant/constant';
import SignIn from './Pages/SignIn/SignIn';
import SignUp from './Pages/SignUp/SignUp';
import AppLayout from './HOC/AppLayout';
import ProjectDashBoard from './Pages/Project/ProjectDashBoard/ProjectDashBoard';
import ProjectManagement from './Pages/Project/ProjectManagement/ProjectManagement';
import CreateProject from './Pages/Project/CreateProject/CreateProject';
import DemoDragDrop from './Pages/Project/DemoDragDrop/DemoDragDrop';
import DragAndDropDnD from './Pages/Project/DragAndDropDnD/DragAndDropDnD';
import NotFoundPage from './Pages/NotFoundPage/NotFoundPage';

function App() {
    return (
        <div className="App">
            <BrowserRouter>
                <Routes>
                    <Route path={`${SIGN_IN}`} element={<SignIn />} />
                    <Route path={`${SIGN_UP}`} element={<SignUp />} />
                    <Route path={`${PROJECT_DASHBOARD}/:projectId`} element={<AppLayout><ProjectDashBoard /></AppLayout>} />
                    <Route path={`${PROJECT_MANAGEMENT}`} element={<AppLayout><ProjectManagement /></AppLayout>} />
                    <Route path={`${CREATE_PROJECT}`} element={<AppLayout><CreateProject /></AppLayout>} />
                    <Route path={NOT_FOUND_PAGE} element={<NotFoundPage />}></Route>
                    <Route path={"dragDrop"} element={<AppLayout><DemoDragDrop /></AppLayout>}></Route>
                    <Route path={"dnd"} element={<AppLayout><DragAndDropDnD /></AppLayout>}></Route>
                </Routes>
            </BrowserRouter>
        </div>
    );
}

export default App;
