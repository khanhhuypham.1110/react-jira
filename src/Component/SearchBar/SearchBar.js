
import { AutoComplete } from 'antd';
import { useRef } from 'react';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { openNotification } from '../../redux/notificationReducer/notificationReducer';
import { projectService } from '../../services/projectService';
import { userService } from '../../services/userService';
import { NOTIFICATION_ERROR, NOTIFICATION_SUCCESS } from '../../utils/constant/constant';


export default function SearchBar(props) {
    const [value, setValue] = useState()
    const [options, setOptions] = useState([]);
    const dispatch = useDispatch()
    const searchRef = useRef(null)
    const fetchProjectList = useSelector(state => state.projectReducer.callBackFetchProjectList)
    const onSearch = (searchText) => {
        let result = [];
        setValue(searchText)
        if (searchRef.current) {
            clearTimeout(searchRef.current)
        }
        searchRef.current = setTimeout(() => {
            userService.getUserWithCondition(searchText).then((res) => {
                result = res.data.content.map((mem) => ({
                    value: JSON.stringify(mem),
                    label: (
                        <div className='flex justify-between'>
                            {mem.name.length <= 20 ? <p>{mem.name}</p> : <p>{mem.name.slice(0, 20)}...</p>}
                            <img src={mem.avatar} className="w-5 h-5 rounded-full" alt="" />
                        </div>

                    )
                }))
                console.log(result);
                setOptions(result);
            }).catch((err) => {
                console.log(err);
            })
        }, 300)
    };

    const onSelect = (data) => {
        let user = JSON.parse(data);
        let projectUser = { userId: user.userId, projectId: props.projectId }
        setValue(user.name)
        projectService.postAssignProjectUser(projectUser).then((res) => {
            console.log(res.data.content);
            dispatch(openNotification({ type: NOTIFICATION_SUCCESS, description: "Add member successfully" }))
            fetchProjectList()
            // navigate(PROJECT_MANAGEMENT)
        }).catch((err) => {
            console.log(err);
            dispatch(openNotification({ type: NOTIFICATION_ERROR, description: err.response.data.content }))
        })
    };

    return (
        <AutoComplete
            value={value}
            allowClear={true}
            options={options}
            style={{
                width: 200,
            }}
            // showSearch
            onSelect={onSelect}
            onSearch={onSearch}
            placeholder="input here"
        />
    );
}
