import { Button, Drawer, Space } from 'antd';
import React, { useEffect, useState } from 'react'
import { AiOutlinePlus } from 'react-icons/ai';
import { useDispatch, useSelector } from 'react-redux';
import { setPriorityList } from '../../../redux/priorityReducer/priorityReducer';
import { setProjectList } from '../../../redux/projectReducer/projectReducer';
import { setStatusList } from '../../../redux/statusReducer/statusReducer';
import { setTaskCallBackSubmit } from '../../../redux/taskReducer/taskReducer';
import { setTaskTypeList } from '../../../redux/taskTypeReducer/taskTypeReducer';
import { setUserSearchList } from '../../../redux/userReducer/userReducer';
import { priorityService } from '../../../services/priorityService';
import { projectService } from '../../../services/projectService';
import { statusService } from '../../../services/statusService';
import { taskTypeService } from '../../../services/taskType';
import { userService } from '../../../services/userService';
import FormCreateTask from '../FormCreateTask';

export default function CreateTaskModal() {
    const [open, setOpen] = useState(false);
    const [placement, setPlacement] = useState('right');
    const taskCallBackSubmit = useSelector(state => state.taskReducer.taskCallBackSubmit)
    const dispatch = useDispatch()

    useEffect(() => {
        projectService.getAllProject().then((res) => {
            // console.log("projectService");
            dispatch(setProjectList(res.data.content))
            // call api getUserByProjectId to set default value for listUserAssign
            userService.getUserByProjectId(res.data.content[0].id).then((response) => {
                // console.log("userService");
                dispatch(setUserSearchList(response.data.content))
            }).catch(error => { console.log(error); })
        }).catch(err => { console.log(err); })

        taskTypeService.getAllTaskType().then((res) => {
            // console.log("taskTypeService");
            dispatch(setTaskTypeList(res.data.content))
        }).catch(err => { console.log(err); })

        priorityService.getAllPriority().then((res) => {
            // console.log("priorityService");
            dispatch(setPriorityList(res.data.content))
        }).catch((err) => { console.log(err); })

        statusService.getAllStatus().then((res) => {
            // console.log("statusService");
            dispatch(setStatusList(res.data.content))
        }).catch((err) => { console.log(err); })
    }, [])


    const showDrawer = () => {
        setOpen(true);
    };
    const onClose = () => {
        setOpen(false)
    };
    const onSubmit = () => {
        taskCallBackSubmit()
        setTimeout(() => {
            setOpen(false)
        }, 800)
    }
    return (
        <>
            <button onClick={showDrawer} className="flex items-center ">
                <span className='mr-5 text-3xl'><AiOutlinePlus></AiOutlinePlus></span>
                <span>CREATE ISSUES</span>
            </button>
            <Drawer
                title="Create Task"
                placement="right"
                width={500}
                onClose={onClose}
                open={open}
                extra={
                    <Space>
                        <Button onClick={onClose}>Cancel</Button>
                        <Button onClick={onSubmit}>
                            Submit
                        </Button>
                    </Space>
                }
            >
                <FormCreateTask />
            </Drawer>
        </>
    );
}
