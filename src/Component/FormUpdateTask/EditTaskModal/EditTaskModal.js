import { Button, Modal } from 'antd';
import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { closePopup } from '../../../redux/popupReducer/popupReducer';
import { closeEditTaskModal, setTaskDetail } from '../../../redux/taskReducer/taskReducer';
import FormUpdateTask from '../FormUpdateTask';

export default function EditTaskModal() {
    const isModalOpen = useSelector(state => state.taskReducer.isModalOpen)
    const { taskCallBackSubmit } = useSelector(state => state.taskReducer)
    const dispatch = useDispatch()
    const handleOk = () => {
        taskCallBackSubmit()
        dispatch(closeEditTaskModal())
        dispatch(closePopup())
    };
    const handleCancel = () => {
        dispatch(setTaskDetail({}))
        dispatch(closeEditTaskModal())
        dispatch(closePopup())
    };
    return (
        <>
            <Modal
                width={600}
                title="Basic Modal"
                open={isModalOpen}
                onOk={handleOk}
                onCancel={handleCancel}
                footer={[
                    <Button key="back" onClick={handleCancel}>
                        Return
                    </Button>,
                    <Button key="submit" onClick={handleOk}>
                        Update
                    </Button>,
                ]}
            >
                <FormUpdateTask />
            </Modal>
        </>
    );
}
