
import { useSelector } from 'react-redux'

export default function Popup() {
    let popup = useSelector(state => state.popupReducer.popup)
    return (popup)
}
