import { Input, message } from 'antd';
import { Editor } from '@tinymce/tinymce-react';
import { useFormik } from 'formik';
import React, { useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import * as Yup from 'yup';
import { openNotification } from '../../redux/notificationReducer/notificationReducer';
import { projectService } from '../../services/projectService';
import { NOTIFICATION_ERROR, NOTIFICATION_SUCCESS, PROJECT_MANAGEMENT } from '../../utils/constant/constant';
export default function FormCreateProject() {
    const category = useSelector(state => state.projectCategoryReducer.projectCategory)
    const { callBackFetchProjectList } = useSelector(state => state.projectReducer)
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const editorRef = useRef(null);
    const formik = useFormik({
        initialValues: {
            projectName: "",
            categoryName: category[0].projectCategoryName,
            description: ""
        },
        validationSchema: Yup.object().shape({
            projectName: Yup.string().required("Project name is required"),
        }),

        onSubmit: (values) => {
            console.log(values);
            if (editorRef.current) {
                let newProject = { ...values, description: editorRef.current.getContent() }
                projectService.postCreateProject(newProject).then((res) => {
                    callBackFetchProjectList()
                    dispatch(openNotification({ type: NOTIFICATION_SUCCESS, description: "created successfully" }))
                    setTimeout(() => { navigate(PROJECT_MANAGEMENT) }, 1000)
                }).catch((err) => {
                    console.log(err);
                    dispatch(openNotification({ type: NOTIFICATION_ERROR, description: err.res.data.content }))
                })

            }

        },
    })

    return (
        <div className='text-left'>
            <form onSubmit={formik.handleSubmit}>
                <div>
                    <label className='space-y-2'>
                        <strong>Project name</strong>
                        <Input
                            className='px-3 py-2 text-sm'
                            onChange={formik.handleChange}
                            type="text"
                            name="projectName" />
                    </label>
                    {formik.errors.projectName && formik.touched.projectName ? <span className='text-red-600'>{formik.errors.projectName}</span> : <></>}
                </div>
                <div>
                    <select
                        name="categoryName"
                        className="w-full mt-5 px-3 py-2 bg-white border shadow-sm border-slate-300
                        focus:outline-none focus:border-sky-500 focus:ring-sky-500 rounded-md text-sm"
                    >
                        {category.map((c, index) => {

                            return index === 0
                                ?
                                <option value={c.projectCategoryName} selected={c.projectCategoryName}> {c.projectCategoryName}</option>
                                :
                                <option value={c.projectCategoryName}> {c.projectCategoryName}</option>

                        })}
                    </select>
                </div>

                <div className='space-y-2 mt-5'>
                    <strong>Description</strong>
                    <Editor
                        apiKey='tcvcx7qqseyscb3adpxwafgjzi507hmvgz2e4823v9jokv5u'
                        name="description"
                        onInit={(evt, editor) => editorRef.current = editor}
                        initialValue="<p>This is the initial content of the editor.</p>"
                        init={{
                            height: 500,
                            menubar: false,
                            plugins: [
                                'advlist autolink lists link image charmap print preview anchor',
                                'searchreplace visualblocks code fullscreen',
                                'insertdatetime media table paste code help wordcount'
                            ],
                            toolbar: 'undo redo | formatselect | ' +
                                'bold italic backcolor | alignleft aligncenter ' +
                                'alignright alignjustify | bullist numlist outdent indent | ' +
                                'removeformat | help',
                            content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
                        }}
                    />
                </div>
                <div className='mt-5 flex justify-start'>
                    <button type='submit' className='px-3 py-1 border-2 rounded-md' >Create</button>
                </div>
            </form>
        </div>
    )
}
