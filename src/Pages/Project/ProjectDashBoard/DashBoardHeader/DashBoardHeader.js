import { Avatar, Breadcrumb, Tooltip } from 'antd'
import React from 'react'
import { PROJECT_MANAGEMENT } from '../../../../utils/constant/constant'

import { NavLink } from 'react-router-dom';
import { GiAnt, GiCamel, GiCampingTent } from 'react-icons/gi';
import parse from 'html-react-parser';

import SearchInput from '../../../../Component/SearchInput/SearchInput';
import PopHover from '../../../../Component/PopHover/PopHover';

export default function DashBoardHeader(props) {
    return (
        <div className='space-y-3'>
            <div className="header">
                <Breadcrumb>
                    <Breadcrumb.Item><NavLink to={PROJECT_MANAGEMENT}>Project Management</NavLink></Breadcrumb.Item>
                    <Breadcrumb.Item><strong>{props.project.projectName}</strong></Breadcrumb.Item>
                </Breadcrumb>
            </div>

            <div className='text-left'>
                {parse(props.project.description)}
            </div>

            <div className="flex items-center">
                <div>
                    <SearchInput />
                </div>

                <div className="flex gap-2 ml-4">
                    {props.project.members.length !== 0
                        ?
                        <PopHover />
                        :
                        <>
                            <div>
                                <GiAnt className='w-[40px] h-[40px] rounded-full' />
                            </div>
                            <div >
                                <GiCamel className='w-[50px] h-[50px] rounded-full' />
                            </div>
                            <div>
                                <GiCampingTent className='w-[40px] h-[40px] rounded-full' />
                            </div>
                        </>
                    }
                </div>
                <div className='space-x-5 ml-5'>
                    <span>Only My Issues</span>
                    <span>Recently Updated</span>
                </div>
            </div>
        </div >
    )
}

