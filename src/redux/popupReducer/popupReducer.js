import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    popup: <div></div>,
    enablePopUp: false,
}

export const popupSlice = createSlice({
    name: "popupSlice",
    initialState: initialState,
    reducers: {
        openPopup: (state, action) => {
            state.popup = action.payload
            state.enablePopUp = true
        },
        closePopup: (state, action) => {
            state.popup = <div></div>
            state.enablePopUp = false
            console.log("popup");
        }
    }
})

export const { openPopup, closePopup } = popupSlice.actions
