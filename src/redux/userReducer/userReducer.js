import { createSlice } from "@reduxjs/toolkit"
import { localStorageService } from "../../services/localStorageService"

const initialState = {
    userInfor: localStorageService.get(),
    userSearchList: [],
    userList: []
}

export const userSlice = createSlice({
    name: "userSlice",
    initialState: initialState,
    reducers: {
        setUserInfor: (state, action) => {
            state.userInfor = action.payload
        },
        setUserSearchList: (state, action) => {
            // console.log("setUserSearchList");
            state.userSearchList = action.payload
        }
    }
})

export const { setUserInfor, setUserSearchList } = userSlice.actions

