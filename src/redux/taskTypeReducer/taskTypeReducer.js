import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    taskTypeList: []
}

export const tasktypeSlice = createSlice({
    name: "taskTypeSlice",
    initialState: initialState,
    reducers: {
        setTaskTypeList: (state, action) => {
            // console.log("setTaskTypeList");
            state.taskTypeList = action.payload
        }
    }
})

export const { setTaskTypeList } = tasktypeSlice.actions

