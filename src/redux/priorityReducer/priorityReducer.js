import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    priorityList: []
}

export const prioritySlice = createSlice({
    name: "prioritySlice",
    initialState: initialState,
    reducers: {
        setPriorityList: (state, action) => {
            // console.log("setPriorityList");
            state.priorityList = action.payload
        }
    }
})

export const { setPriorityList } = prioritySlice.actions