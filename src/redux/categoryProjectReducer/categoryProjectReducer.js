import { createSlice } from "@reduxjs/toolkit"


const initialState = {
    projectCategory: []
}


export const projectCategorySlice = createSlice({
    name: "categoryProjectSlice",
    initialState: initialState,
    reducers: {
        setProjectCategory: (state, action) => {
            state.projectCategory = action.payload
        }
    }
})

export const { setProjectCategory } = projectCategorySlice.actions
