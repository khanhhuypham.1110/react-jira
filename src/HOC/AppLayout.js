import React, { useEffect, useRef, useState } from 'react';
import { SiSemanticuireact } from "react-icons/si"
import { AiOutlineFileSearch, AiOutlineQuestionCircle } from "react-icons/ai"
import { MdCommute, MdLogout, MdOutlineDashboardCustomize, MdOutlineFileCopy, MdStorage } from "react-icons/md"
import { BsPencilSquare, BsTelegram } from "react-icons/bs"
import { VscNewFile } from "react-icons/vsc"
import { DiDropbox } from "react-icons/di"
import { MenuFoldOutlined, MenuUnfoldOutlined } from '@ant-design/icons';
import { Layout, Menu, theme } from 'antd';
import { NavLink } from 'react-router-dom';
import Sider from 'antd/es/layout/Sider';
import { Content, Header } from 'antd/es/layout/layout';
import { CREATE_PROJECT, NOTIFICATION_WARNING, PROJECT_DASHBOARD, PROJECT_MANAGEMENT, SIGN_IN } from '../utils/constant/constant';
import { localStorageService } from "../services/localStorageService"
import { projectService } from '../services/projectService';
import { useDispatch } from 'react-redux';
import { setProjectCategory } from '../redux/categoryProjectReducer/categoryProjectReducer';
import Notification from '../Component/Notification/Notification';
import Popup from '../Component/Popup/Popup';
import CreateTaskModal from '../Component/FormCreateTask/CreateTaskModal/CreateTaskModal';
import { store } from '../redux/store';
import { openNotification } from '../redux/notificationReducer/notificationReducer';


export default function AppLayout({ children }) {

    const [collapsed, setCollapsed] = useState(false);
    const { token: { colorBgContainer }, } = theme.useToken();
    const dispatch = useDispatch()
    const refContainer = useRef()
    const [dimensions, setDimensions] = useState({ width: 0, height: 0 })
    const logout = () => {
        localStorageService.remove()
        window.location.href = `${SIGN_IN}`
    }
    useEffect(() => {
        if (refContainer.current) {
            setDimensions({
                width: refContainer.current.offsetWidth,
                height: refContainer.current.offsetHeight
            })
        }
        projectService.getProjectCategory().then((res) => {
            dispatch(setProjectCategory(res.data.content))
        })
    }, [children])
    return (
        <div>
            <Layout>
                <div className='bg-blue-600 text-white w-[70px] h-full p-5 fixed z-20 grid grid-rows-2 cursor-pointer overflow-hidden ease-in-out duration-1000 hover:w-[200px]'>
                    <div className='space-y-8 min-w-[170px] flex flex-col justify-center'>
                        <div className="text-3xl">
                            <SiSemanticuireact></SiSemanticuireact>
                        </div>
                        <div className="flex items-center  " >
                            <span className='mr-5 text-3xl'><AiOutlineFileSearch></AiOutlineFileSearch></span>
                            <span>SEARCH ISSUES</span>
                        </div>
                        <div>
                            <CreateTaskModal />
                        </div>
                    </div>
                    <div className="flex items-center">
                        <span className='mr-5 text-3xl'><AiOutlineQuestionCircle></AiOutlineQuestionCircle></span>
                        <span >ABOUT</span>

                    </div>
                </div>
                <Sider trigger={null} collapsible collapsed={collapsed} className='ml-[70px]' >

                    <div>
                        <div className='bg-neutral-100' >
                            <img src="https://blog.logomyway.com/wp-content/uploads/2021/11/meta-logo.png" alt='' className='h-auto object-fill' />
                        </div>
                    </div>
                    <Menu
                        style={{ height: 700 }}
                        theme="dark"
                        mode="inline"
                        defaultSelectedKeys={[PROJECT_DASHBOARD]}
                        items={[
                            {
                                key: PROJECT_DASHBOARD,
                                icon: <MdOutlineDashboardCustomize className='text-white' />,
                                label: (
                                    <NavLink to={`${PROJECT_MANAGEMENT}`} onClick={() => {
                                        store.dispatch(openNotification({ type: NOTIFICATION_WARNING, description: "please, choose a specific project for display of project dashboard" }))
                                    }}>
                                        <span>project dashboard</span>
                                    </NavLink>
                                ),
                            },
                            {
                                key: PROJECT_MANAGEMENT,
                                icon: <BsPencilSquare />,
                                label: (
                                    <NavLink to={`${PROJECT_MANAGEMENT}`}>
                                        <span>Project Management</span>
                                    </NavLink>
                                ),
                            },
                            {
                                key: CREATE_PROJECT,
                                icon: <VscNewFile />,
                                label: (
                                    <NavLink to={`${CREATE_PROJECT}`} className="before:border-b-2">
                                        <span>Create Project</span>
                                    </NavLink>),
                            },

                            {
                                key: 'RELEASES',
                                icon: <MdCommute />,
                                label: (
                                    <NavLink to="">
                                        <span>Releases</span>
                                    </NavLink>),
                            },

                            {
                                key: 'ISSUES_AND_FILTER',
                                icon: <MdStorage />,
                                label: (
                                    <NavLink to="">
                                        <span>Issues and filter</span>
                                    </NavLink>),
                            },

                            {
                                key: 'PAGES',
                                icon: <MdOutlineFileCopy />,
                                label: (
                                    <NavLink to="">
                                        <span>Pages</span>
                                    </NavLink>),
                            },

                            {
                                key: 'REPORT',
                                icon: <BsTelegram />,
                                label: (
                                    <NavLink to="">
                                        <span>Report</span>
                                    </NavLink>),
                            },

                            {
                                key: 'COMPONENT',
                                icon: <DiDropbox />,
                                label: (
                                    <NavLink to="">
                                        <span>Component</span>
                                    </NavLink>),
                            },
                        ]}
                    />
                </Sider>

                <Layout className="site-layout">
                    <Header
                        style={{
                            padding: 0,
                            background: colorBgContainer,
                        }}
                    >
                        <div className='flex justify-between items-center h-full'>
                            <>
                                {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                                    className: 'trigger',
                                    onClick: () => setCollapsed(!collapsed),
                                })}
                            </>
                            <button onClick={() => { logout() }} className="h-10 py-1 px-3 border-2 border-sky-200 rounded-md flex justify-between items-center gap-2 mr-5">
                                <MdLogout /><span>logout</span>
                            </button>
                        </div>
                    </Header>
                    <Content
                        style={{
                            margin: '24px 16px',
                            padding: 24,
                            height: "auto",
                            background: colorBgContainer,
                        }}
                        ref={refContainer}
                    >
                        <Notification />
                        <Popup />
                        {children}
                    </Content>
                </Layout>
            </Layout >
        </div >
    );
}



