import { http } from "./configURL"

export const statusService = {
    getAllStatus: () => {
        return http.get("/api/Status/getAll")
    },
}