import axios from "axios"
import { localStorageService } from "./localStorageService"


const TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCBTw6FuZyAwNSIsIkhldEhhblN0cmluZyI6IjI4LzA1LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY4NTIzMjAwMDAwMCIsIm5iZiI6MTY2MjMxMDgwMCwiZXhwIjoxNjg1Mzc5NjAwfQ.FtGbsXl4qyqTRfJrunro0mQ7b-tNs8EWbhb7JDTzloE"
const BASE_URL = "https://jiranew.cybersoft.edu.vn"
const config = () => {
    return {
        TokenCybersoft: TOKEN,
        Authorization: "Bearer " + localStorageService.get()?.accessToken
    }
}

export const http = axios.create({
    baseURL: BASE_URL,
    headers: config(),
})


