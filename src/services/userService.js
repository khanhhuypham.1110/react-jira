import { http } from "./configURL"

export const userService = {
    signIn: (userInfor) => {
        return http.post("/api/Users/signin", userInfor)
    },

    signUp: (userInfor) => {
        return http.post("/api/Users/signup", userInfor)
    },

    getUserWithCondition: (condition) => {
        return http.get(`/api/Users/getUser?keyword=${condition}`)
    },

    getUserByProjectId: (projectId) => {
        return http.get(`/api/Users/getUserByProjectId?idProject=${projectId}`)
    }
}