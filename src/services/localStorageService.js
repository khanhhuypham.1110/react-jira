import { USER_INFOR } from "../utils/constant/constant"

export const localStorageService = {
    get: () => {
        const userInforJson = localStorage.getItem(USER_INFOR)
        if (userInforJson !== null) {
            return JSON.parse(userInforJson)
        } else {
            return null
        }

    },
    set: (userInfor) => {
        const userInforJson = JSON.stringify(userInfor)
        localStorage.setItem(USER_INFOR, userInforJson)
    },
    remove: () => {
        localStorage.removeItem(USER_INFOR)
    }
}