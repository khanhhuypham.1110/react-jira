import { http } from "./configURL"
export const commentService = {
    getAllComment: (taskId) => {
        return http.get(`api/Comment/getAll?taskId=${taskId}`)
    }
}