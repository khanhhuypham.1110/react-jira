import { http } from "./configURL"

export const taskTypeService = {
    getAllTaskType: () => {
        return http.get("api/TaskType/getAll")
    }
}